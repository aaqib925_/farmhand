import React from 'react';
import { primaryColor } from './src/Constants/styles'
import { Provider } from "react-redux";
import StatusBarComponent from "./src/Components/SharedComponents/StatusBarComponent"
import MainRoute from './src/Components/Routes/MainRoute';
import { store } from "./src/Redux/Store"
const App = () => {
  console.disableYellowBox = true; // For disabling warning yellow box

  return (
    <>
      <Provider store={store}>
        <StatusBarComponent barStyle="light-content" backgroundColor={primaryColor} />
        <MainRoute />
        {/* <ToastComponent /> */}
      </Provider>
    </>

  )
}

export default App