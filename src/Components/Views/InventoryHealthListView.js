import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { fontFamily, fontH3V3, textPrimayColor, textSecondaryColor, } from '../../Constants/styles'
import { normalizeFont, normalizeHeight, normalizeWidth } from '../../Utils/fontUtil'

const InventoryHealthListView = ({ item }) => {
    console.log("item", item)
    return (
        <View style={{ flex: 1, flexDirection: 'row', paddingVertical: normalizeHeight(20), borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: textSecondaryColor }}>
            <View style={{ flex: 1, marginTop: normalizeHeight(2) }}>
                <View style={{ width: normalizeWidth(5), height: normalizeWidth(5), backgroundColor: item.color, borderRadius: normalizeWidth(5) }}>
                </View>
            </View>
            <View style={{ flex: 7, }}>
                <Text style={{ fontFamily: fontFamily.Primary.Heavy, fontSize: fontH3V3, color: textPrimayColor }}>{item.name}</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ fontFamily: fontFamily.Primary.Regular, fontSize: normalizeFont(8), color: textSecondaryColor }}>{item.time}</Text>
                    <Text style={{ fontFamily: fontFamily.Primary.Medium, fontSize: normalizeFont(8), color: "#2080BD" }}>{item.type}</Text>
                </View>
            </View>
        </View>
    )
}

export default InventoryHealthListView

const styles = StyleSheet.create({})
