import React from "react";
import { View, Text, Image, StyleSheet, FlatList, TouchableOpacity } from "react-native"
import BackHeader from "../Shared/BackHeader";
import ViewHeader from "../Shared/ViewHeader";
import { normalizeFont, normalizeHeight, normalizeWidth } from "../../../Utils/fontUtil";
import { containerShadow, fontFamily, fontH1, fontH2, fontH2V3, fontH3, fontH3V3, secondaryColor } from "../../../Constants/styles";
import { HorseImage } from "../../../assets/images";
import { AnimalInventoryData, inventoryHealthData, InventoryReminderData, inventoryVideoArchives } from "../../../Constants/enum";
import AnimalInventoryDetailsListView from "../Shared/AnimalInventoryDetailsListView";
import CustomButton from "../../SharedComponents/CustomButton";
import AnimalInventoryVideosListView from "../Shared/AnimalInventoryVideosListView";
import AddPlaceHolder from "../Shared/AddPlaceHolder";
import AnimalInventoryReminderListView from "../Shared/AnimalInventoryReminderListView"
import InventoryHealthListView from "../InventoryHealthListView";
import { RouteNames } from "../../../Constants/routeNames";
import CustomAddPlaceholder from "../Shared/CustomAddPlaceholder";
const AnimalInventoryScreen = ({ navigation }) => {
    const renderItem = ({ item }) => {
        return (
            <AnimalInventoryDetailsListView item={item} />
        )
    }
    const RenderPlaceHolder = () => {
        return (
            <View>
                <AddPlaceHolder
                    onPress={onPress}
                    imageSize={15}
                    customStyles={{
                        width: normalizeWidth(60),
                        height: normalizeWidth(35),
                        marginRight: normalizeWidth(5),
                        marginTop: normalizeHeight(7),
                        borderRadius: normalizeWidth(2),
                    }}
                />
            </View>
        )
    }
    const onPress = () => {
        navigation.navigate(RouteNames.User.AddVideo)
    }
    const renderVideoItems = ({ item }) => {
        return (
            item.showAdd ? <RenderPlaceHolder /> : < AnimalInventoryVideosListView item={item} />
        )
    }
    const renderInventoryReminder = ({ item }) => {
        return (
            <AnimalInventoryReminderListView item={item} />
        )
    }
    const renderInventoryHealth = ({ item }) => {
        return (
            <InventoryHealthListView item={item} />
        )
    }
    return (
        <ViewHeader>
            <BackHeader heading={'Horse Inventory'} />
            <View style={{ flex: 1, flexDirection: 'row', padding: normalizeWidth(5) }}>
                <View style={styles.firstContainer}>
                    <View style={{ flex: 1, paddingHorizontal: normalizeWidth(7) }}>
                        <View style={{ flex: 3, }}>
                            <Text style={{ fontFamily: fontFamily.Primary.Heavy, fontSize: fontH2V3 }}>Personal Details</Text>
                            <Image source={HorseImage} style={{ width: "100%", height: normalizeWidth(70), marginTop: normalizeHeight(20) }} resizeMode="contain" />
                            <View style={{ marginTop: normalizeHeight(10) }}>
                                <FlatList
                                    numColumns={2}
                                    keyExtractor={(item, index) => item.id}
                                    data={AnimalInventoryData}
                                    renderItem={renderItem}
                                    columnWrapperStyle={{ marginTop: normalizeHeight(5), flexDirection: 'row', justifyContent: 'space-between' }}
                                />
                            </View>
                        </View>
                        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                            <CustomButton buttonColor="white" buttonText={"Certificates"} />
                            <CustomButton buttonTextColor="white" buttonText={"AQUA SHOWS"} />
                        </View>
                    </View>
                </View>
                <View style={styles.secondContainer}>
                    <View style={styles.topRightContainer}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flex: 1, flexDirection: 'row', paddingVertical: normalizeHeight(20), justifyContent: 'space-between', paddingHorizontal: normalizeWidth(7), alignItems: 'center' }}>
                                <Text style={{ fontFamily: fontFamily.Primary.Heavy, fontSize: fontH2V3 }}>Video Archives</Text>
                                <TouchableOpacity onPress={() => navigation.navigate(RouteNames.User.VideoArchieve)}>
                                    <Text style={{ fontFamily: fontFamily.Primary.Medium, fontSize: fontH3V3, color: secondaryColor }}>View All</Text>

                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flex: 1, paddingHorizontal: normalizeWidth(5), justifyContent: 'center' }}>
                            <FlatList
                                keyExtractor={(item, index) => item.id}
                                renderItem={renderVideoItems}
                                data={inventoryVideoArchives}
                                horizontal={true}
                            />
                        </View>
                    </View>
                    <View style={{ flex: 2, ...containerShadow, flexDirection: 'row' }}>
                        <View style={{ flex: 1.2, backgroundColor: 'white', marginRight: normalizeWidth(7), ...containerShadow }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View style={{ flex: 1, flexDirection: 'row', paddingVertical: normalizeHeight(20), justifyContent: 'space-between', paddingHorizontal: normalizeWidth(7), alignItems: 'center' }}>
                                    <Text style={{ fontFamily: fontFamily.Primary.Heavy, fontSize: fontH2V3 }}>Reminders</Text>
                                    <Text style={{ fontFamily: fontFamily.Primary.Medium, fontSize: fontH3V3, color: secondaryColor }}>View All</Text>
                                </View>
                            </View>
                            <View style={{ paddingHorizontal: normalizeWidth(10) }}>
                                <FlatList
                                    data={InventoryReminderData}
                                    keyExtractor={(item, index) => item.id}
                                    renderItem={renderInventoryReminder}
                                />
                            </View>
                        </View>
                        <View style={{ flex: 1, backgroundColor: 'white', ...containerShadow }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View style={{ flex: 1, flexDirection: 'row', paddingVertical: normalizeHeight(20), justifyContent: 'space-between', paddingHorizontal: normalizeWidth(7), alignItems: 'center' }}>
                                    <Text style={{ fontFamily: fontFamily.Primary.Heavy, fontSize: fontH2V3 }}>Health</Text>
                                    <Text style={{ fontFamily: fontFamily.Primary.Medium, fontSize: fontH3V3, color: secondaryColor }}>View All</Text>
                                </View>
                            </View>
                            <View style={{ paddingHorizontal: normalizeWidth(10) }}>
                                <FlatList
                                    data={inventoryHealthData}
                                    keyExtractor={(item, index) => item.id}
                                    renderItem={renderInventoryHealth}
                                />
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        </ViewHeader >
    )
}
const styles = StyleSheet.create({
    firstContainer: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        marginRight: normalizeWidth(10),
        borderRadius: normalizeWidth(3),
        ...containerShadow,
        paddingVertical: normalizeHeight(20),
    },
    secondContainer: {
        flex: 2,
    },
    topRightContainer: {
        flex: 1.5,
        backgroundColor: 'white',
        marginBottom: normalizeWidth(7),
        ...containerShadow,
        borderRadius: normalizeWidth(3),
    }
})
export default AnimalInventoryScreen;