import React, { useState } from 'react'
import { FlatList, StyleSheet, Text, View } from 'react-native'
import { SettingsMenu } from '../../../Constants/enum'
import { containerShadow } from '../../../Constants/styles'
import { normalizeWidth } from '../../../Utils/fontUtil'
import BackHeader from '../Shared/BackHeader'
import ViewHeader from '../Shared/ViewHeader'
import EmailSettings from './Settings/EmailSettings'
import LanguageSettings from './Settings/LanguageSettings'
import PhoneSettings from './Settings/PhoneSettings'
import SettingsListview from './Settings/SettingsListview'

const SettingsScreen = () => {
    const [currentItem, setCurrentItem] = useState(SettingsMenu[0].id)
    const onPress = (itemId) => {
        setCurrentItem(itemId)
    }
    const renderItem = ({ item }) => {
        return (
            <SettingsListview currentItem={currentItem} id={item.id} onPress={onPress} title={item.title} isLast={item.isLast} />
        )
    }
    const getRightView = () => {
        return (
            <>
                {currentItem === 1 ? <EmailSettings /> : currentItem === 2 ? <PhoneSettings /> : <LanguageSettings />}
            </>
        )
    }
    return (
        <ViewHeader>
            <BackHeader heading={'Settings'} />
            <View style={{ flex: 1, flexDirection: 'row' }}>
                <View style={{ flex: 1, marginRight: normalizeWidth(5) }}>
                    <View style={{ backgroundColor: 'white', ...containerShadow, borderRadius: normalizeWidth(3) }}>
                        <FlatList
                            data={SettingsMenu}
                            keyExtractor={(item, index) => item.id}
                            renderItem={renderItem}
                        />
                    </View>
                </View>
                <View style={{ flex: 2, }}>
                    {getRightView()}
                </View>
            </View>
        </ViewHeader>
    )
}

export default SettingsScreen

const styles = StyleSheet.create({})
