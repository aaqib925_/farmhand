import React from "react"
import { View, StyleSheet, ImageBackground, Text, Image, FlatList } from "react-native"
import { HorseHeaderBackgroundImage, HorseImage, HorseVideoIcon, OrangePlusIcon } from "../../../assets/images"
import { DummyCertificates } from "../../../Constants/enum"
import { containerShadow, fontFamily, fontH2, fontH3, textPrimayColor } from "../../../Constants/styles"
import { normalizeHeight, normalizeWidth, normalizeWithScale } from "../../../Utils/fontUtil"
import ImageContainer from "../../SharedComponents/ImageContainer"
import BackHeader from "../Shared/BackHeader"
import ViewHeader from "../Shared/ViewHeader"
import CertificatesListView from "../Shared/CertificatesListView"
const KeyValueText = ({ keyOf, value }) => {
    return (
        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <Text style={{ color: '#FFFFFF97', fontFamily: fontFamily.Primary.Light, fontSize: fontH3, marginRight: normalizeWidth(5) }}>{keyOf}</Text>
            <Text style={{ color: 'white', fontFamily: fontFamily.Primary.Bold, fontSize: fontH3 }}>{value}</Text>
        </View>
    )
}

const CertificateScreen = () => {
    const renderItem = ({ item }) => {
        return (
            <CertificatesListView item={item} />
        )
    }
    return (
        <View style={{ flex: 1, }}>
            <View style={{ flex: 1, borderBottomRightRadius: normalizeWidth(30), borderBottomLeftRadius: normalizeWidth(30), overflow: 'hidden' }}>
                <ImageBackground source={HorseVideoIcon} style={{ width: "100%", height: "100%" }}>
                    <View style={{ flex: 2.5, backgroundColor: '#2080BD20', paddingVertical: normalizeWidth(5), paddingHorizontal: normalizeWidth(5) }}>
                        <BackHeader heading={"Max's Certificate"} useWhiteTheme={true} />
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <ImageContainer image={HorseImage} circleWidth={50} borderWidth={0.2} />
                        </View>
                    </View>
                    <View style={{ backgroundColor: '#02020230', flex: 1, flexDirection: 'row', alignItems: "center", justifyContent: 'space-evenly', paddingHorizontal: normalizeWidth(20) }}>
                        <KeyValueText keyOf={"Birthday"} value={"12 March 1999"} />
                        <KeyValueText keyOf={"Registry"} value={"AQHA"} />
                        <KeyValueText keyOf={"Birth Place"} value={"King Ranch"} />
                    </View>
                </ImageBackground>
            </View>
            <View style={{ flex: 1.2, }}>
                <View style={{ flex: 1, backgroundColor: 'white', marginVertical: normalizeWidth(20), marginHorizontal: normalizeWidth(20), ...containerShadow, borderRadius: normalizeWidth(7), paddingHorizontal: normalizeWidth(12) }}>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text style={{ fontFamily: fontFamily.Primary.Heavy, color: textPrimayColor, fontSize: fontH2 }}>Certificates</Text>
                    </View>
                    <View style={{ flex: 3, }}>
                        <View style={{ flex: 1, paddingVertical: normalizeHeight(20) }}>
                            <FlatList
                                data={DummyCertificates}
                                keyExtractor={(item, index) => item.id}
                                renderItem={renderItem}
                                horizontal={true}
                            />
                        </View>
                    </View>
                </View>
                <Image source={OrangePlusIcon} style={{ position: 'absolute', width: normalizeWidth(25), height: normalizeWidth(25), resizeMode: "contain", bottom: normalizeWidth(14), right: normalizeWidth(10) }} />
            </View>
        </View >
    )
}

const styles = StyleSheet.create({
    background: {
        // width: '100%',
        height: "30%",
        borderBottomLeftRadius: 200,
        borderBottomRightRadius: 200,
        // flexDirection: 'row',
        flex: 1
    }
})

export default CertificateScreen;