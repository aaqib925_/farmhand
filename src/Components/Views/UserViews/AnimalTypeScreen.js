import React, { useState } from "react";
import { View, Text, Image, StyleSheet,TouchableOpacity,FlatList } from "react-native"
import BackHeader from "../Shared/BackHeader";
import ViewHeader from "../Shared/ViewHeader";
import { ButtonNextIcon, CowImage, HourseImage, SwineImage } from "../../../assets/images/index"
import { normalizeFont, normalizeHeight, normalizeWidth } from "../../../Utils/fontUtil";
import { fontFamily } from "../../../Constants/styles";
import AnimalBox from "../Shared/AnimalBox";
import { RouteNames } from "../../../Constants/routeNames";
const AnimalTypeScreen = ({navigation}) => {
    const [selectedId, setSelectedId] = useState(1);
    const handleOnAnimalChange = (id) => {
        setSelectedId(id);
    }
    const animalTypesData = [{
        id: 1,
        name: 'Cattle',
        image_url: CowImage
    }, {
        id: 2,
        name: 'Horse',
        image_url: HourseImage
    }, {
        id: 3,
        name: 'Swine',
        image_url: SwineImage
    }]
    const renderAnimalBox = ({ item }) => {
        const { id,name, image_url, onPress } = item;
        return (
            <AnimalBox selectedId={selectedId} id={id} name={name} image_url={image_url} showSelection={true} onPress={handleOnAnimalChange} />
        )
    }
    return (
        <ViewHeader>
            <BackHeader heading={'Select Animal Type'} />
            <View style={{ justifyContent: 'center', flex: 1 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                    <FlatList
                        renderItem={renderAnimalBox}
                        data={animalTypesData}
                        numColumns={3}
                        columnWrapperStyle={{ flexDirection: 'row', justifyContent: 'space-evenly', padding: normalizeWidth(10) }}
                    />
                </View>

            </View>
            <TouchableOpacity onPress={()=> navigation.navigate(RouteNames.User.AnimalDashboard)} style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                <Image style={{
                    width: normalizeWidth(20),
                    height: normalizeWidth(20)
                }} source={ButtonNextIcon} resizeMode='contain' />
            </TouchableOpacity>
        </ViewHeader>
    )
}

export default AnimalTypeScreen;