import React, { useState } from "react"
import { View, StyleSheet, Text, FlatList } from "react-native"
import { containerShadow, fontFamily, fontH2, fontH2V2, fontH3, textPrimayColor } from "../../../Constants/styles"
import { normalizeFont, normalizeHeight, normalizeWidth } from "../../../Utils/fontUtil"
import BackgroundImageHeader from "../../SharedComponents/BackgroundImageHeader"
import { DummyCertificates } from "../../../Constants/enum"
import CertificatesListView from "../Shared/CertificatesListView"
import CustomBoxInput from "../../SharedComponents/BoxInput"
import { validationFieldsKeys } from "../../../Utils/validationUtil"
import { bellWhite } from "../../../assets/images"
const AddCertificateScreen = () => {
    const [registerForm, setRegisterForm] = useState({
        name: { keyName: "name", value: '', error: null, vaidateOption: new validationFieldsKeys({ fieldName: "name", Min: 0, Max: 50 }) },
        certificate: { keyName: "certificate", value: '', error: null, vaidateOption: new validationFieldsKeys({ fieldName: "certificate", Min: 0, Max: 50 }) },
    })
    const handleOnValueChange = (value, FormKeyName) => {
        let formObject = { ...registerForm };
        formObject[`${FormKeyName}`] = { ...formObject[`${FormKeyName}`], value: value, error: null };
        setRegisterForm(formObject);
    }
    const onBlur = (attrName) => {
        let isValid = handleValidateSingleField(registerForm, attrName);
        if (isValid.IsError) {
            let formObject = { ...registerForm };
            formObject[`${attrName}`] = { ...formObject[`${attrName}`], error: isValid.FieldError };
            setRegisterForm(formObject);
        }
    }
    const renderItem = ({ item }) => {
        return (
            <CertificatesListView item={item} width={60} />
        )
    }
    return (
        <BackgroundImageHeader heading="Add Max's Certificates">
            <View style={{ flex: 1, paddingVertical: normalizeWidth(20), paddingHorizontal: normalizeWidth(20) }}>
                <View style={{ flex: 1, backgroundColor: 'white', borderRadius: normalizeWidth(5), ...containerShadow, paddingHorizontal: normalizeWidth(12) }}>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text style={{ fontFamily: fontFamily.Primary.Heavy, color: textPrimayColor, fontSize: fontH2 }}>Certificates</Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ flex: 3, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ width: "40%" }}>
                                <CustomBoxInput
                                    attrName={registerForm.name.keyName}
                                    title={"Name"}
                                    Heading="Certificate Name"
                                    value={registerForm.name.value}
                                    updateMasterState={handleOnValueChange}
                                    onBlur={onBlur}
                                    error={registerForm.name.error}
                                    isRequired={false}
                                    simpleInput={true}
                                    keyboardType={"email-address"}
                                />
                            </View>
                            <View style={{ width: "55%" }}>
                                <CustomBoxInput
                                    attrName={registerForm.certificate.keyName}
                                    title={"Upload Pictures of Certificate "}
                                    Heading="Upload Certificate"
                                    value={registerForm.certificate.value}
                                    updateMasterState={handleOnValueChange}
                                    onBlur={onBlur}
                                    error={registerForm.certificate.error}
                                    isRequired={false}
                                    simpleInput={true}
                                    keyboardType={"email-address"}
                                />
                            </View>
                        </View>
                        <View style={{ flex: 1 }}>
                        </View>
                    </View>
                    <View style={{ flex: 2, }}>
                        <View style={{ flex: 1, paddingVertical: normalizeHeight(10) }}>
                            <Text style={{ color: textPrimayColor, fontFamily: fontFamily.Primary.Medium, fontSize: fontH2V2 }}>Preview</Text>
                            <View style={{ flex: 1, marginBottom: normalizeHeight(20) }}>
                                <FlatList
                                    data={DummyCertificates}
                                    keyExtractor={(item, index) => item.id}
                                    renderItem={renderItem}
                                    horizontal={true}
                                />
                            </View>
                        </View>
                    </View>

                </View>
            </View>
        </BackgroundImageHeader>
    )

}

export default AddCertificateScreen;