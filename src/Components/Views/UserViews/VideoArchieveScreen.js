import React from "react"
import { View, Text, FlatList } from "react-native"
import { dummyVideoArchives, inventoryVideoArchives } from "../../../Constants/enum";
import { containerShadow, fontFamily, textPrimayColor } from "../../../Constants/styles";
import { normalizeFont, normalizeHeight, normalizeWidth } from "../../../Utils/fontUtil";
import AnimalInventoryVideosListView from "../Shared/AnimalInventoryVideosListView";
import BackHeader from "../Shared/BackHeader";
import ViewHeader from "../Shared/ViewHeader";

const VideoArchieveScreen = () => {

    const renderVideoItems = ({ item }) => {
        return (

            <AnimalInventoryVideosListView item={item} />
        )
    }
    return (
        <ViewHeader>
            <BackHeader heading={'Video Archieve'} />
            <View style={{ flex: 1, ...containerShadow, padding: normalizeWidth(15), backgroundColor: '#FFFFFF', marginLeft: normalizeWidth(20), marginRight: normalizeWidth(20), marginBottom: normalizeHeight(40) }}>
                <View style={{ marginBottom: normalizeHeight(60) }}>
                    <Text style={{ color: textPrimayColor, fontFamily: fontFamily.Primary.Heavy, fontSize: normalizeFont(18) }}>Video Archieves</Text>
                </View>
                <FlatList
                    keyExtractor={(item, index) => item.id}
                    renderItem={renderVideoItems}
                    data={dummyVideoArchives}
                    columnWrapperStyle={{ justifyContent: 'space-between' }}
                    numColumns={4}
                    scrollEnabled={true}
                />
            </View>
        </ViewHeader>
    )
}
export default VideoArchieveScreen;