import React from "react"
import { View } from "react-native"
import { FlatList } from "react-native-gesture-handler";
import { CowImage, HourseImage, SwineImage } from "../../../assets/images";
import { RouteNames } from "../../../Constants/routeNames";
import { normalizeHeight, normalizeWidth } from "../../../Utils/fontUtil";
import AddPlaceHolder from "../Shared/AddPlaceHolder";
import AnimalBox from "../Shared/AnimalBox";
import BackHeader from "../Shared/BackHeader";
import CountBox from "../Shared/CountBox";
import CustomAddPlaceholder from "../Shared/CustomAddPlaceholder";
import NotificationTray from "../Shared/NotificationTray";
import ViewHeader from "../Shared/ViewHeader";
const AnimalDashboardScreen = ({ navigation }) => {

    const animalTypesData = [{
        showAdd: true
    }, {
        name: 'Cattle',
        image_url: CowImage,
        onPress: () => navigation.navigate(RouteNames.User.AnimalInventory)
    }, {
        name: 'Horse',
        image_url: HourseImage,
        onPress: () => navigation.navigate(RouteNames.User.AnimalInventory)
    }, {
        name: 'Swine',
        image_url: SwineImage,
        onPress: () => navigation.navigate(RouteNames.User.AnimalInventory)
    }]
    const data = [{
        count: '10',
        name: 'Total Horse',
        backgroundColor: '#2080BD'
    }, {
        count: '02',
        name: 'Pregnent',
        backgroundColor: '#4EB4C4'
    }, {
        count: '04',
        name: 'In Heat',
        backgroundColor: '#955D6E'
    }, {
        count: '06',
        name: 'Farrier',
        backgroundColor: '#3C7921'
    }, {
        count: '03',
        name: 'Breedings',
        backgroundColor: '#F94F4F'
    }, {
        count: '04',
        name: 'Vet Visit',
        backgroundColor: '#F7941D'
    }]
    const renderCountBoxes = ({ item }) => {
        const { count, name, backgroundColor } = item;
        return (
            <CountBox count={count} name={name} backgroundColor={backgroundColor} />
        )
    }
    const renderAnimalBox = ({ item }) => {
        const { name, image_url, showAdd, onPress } = item;
        console.log(name, image_url)
        return (
            showAdd ? <AddPlaceHolder /> :
                <AnimalBox name={name} image_url={image_url} showSelection={false} onPress={onPress} />
        )

    }
    return (
        <ViewHeader>
            <BackHeader heading={'Dashboard'} />
            <View style={{ marginBottom: normalizeHeight(40) }}>
                <FlatList
                    data={data}
                    renderItem={renderCountBoxes}
                    numColumns={6}
                    columnWrapperStyle={{ flexDirection: 'row', justifyContent: 'space-between' }}
                />
            </View>
            <View style={{ marginTop: normalizeHeight(30) }}>
                <NotificationTray />
            </View>
            <View>
                <FlatList
                    renderItem={renderAnimalBox}
                    data={animalTypesData}
                    numColumns={4}
                    columnWrapperStyle={{ flexDirection: 'row', justifyContent: 'space-evenly', paddingVertical: normalizeWidth(20) }}
                />
            </View>
        </ViewHeader>
    )
}
export default AnimalDashboardScreen;