import React, { useMemo } from "react"
import { View, Text, StyleSheet, FlatList } from "react-native"
import { containerShadow, fontFamily } from "../../../Constants/styles";
import { normalizeFont, normalizeHeight, normalizeWidth } from "../../../Utils/fontUtil";
import CustomButton from "../../SharedComponents/CustomButton";
import BackHeader from "../Shared/BackHeader";
import ViewHeader from "../Shared/ViewHeader";
const SubscriptionScreen = () => {
    const SubscriptionBox = ({ planName = 'Starter', amount = 15, duration = 'Month', selected = false }) => {
        return (
            <View style={{ ...containerShadow, backgroundColor: selected ? '#2080BD' : '#ffffff', borderRadius: normalizeWidth(4), paddingVertical: normalizeWidth(15), width: '30%' }}>
                <View style={{ flex: 1, padding: normalizeWidth(10) }}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontFamily: fontFamily.Primary.Medium, fontSize: normalizeFont(18), color: !selected ? '#0E134F' : '#FFFFFF' }}>{planName}</Text>
                    </View>
                    <View style={{ flex: 1.5, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontFamily: fontFamily.Primary.Bold, fontSize: normalizeFont(24), color: !selected ? '#2080BD' : '#FFFFFF' }}>{amount}$</Text>
                        <Text style={{ fontFamily: fontFamily.Primary.Medium, fontSize: normalizeFont(14), color: !selected ? '#0E134F' : '#FFFFFF' }}>/{duration}</Text>
                    </View>
                </View>
                <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, borderColor: '#42526E33' }}></View>
                <View style={{ flex: 3, paddingHorizontal: normalizeWidth(10) }}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: normalizeHeight(40) }} >
                        <Text style={{ color: '#42526E', fontFamily: fontFamily.Primary.Medium, fontSize: normalizeFont(14), color: !selected ? '#0E134F' : '#FFFFFF' }}>Detail Profile</Text>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                        <Text style={{ color: '#42526E', fontFamily: fontFamily.Primary.Medium, fontSize: normalizeFont(14), color: !selected ? '#0E134F' : '#FFFFFF' }}>Tear Conversation</Text>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                        <Text style={{ color: '#42526E', fontFamily: fontFamily.Primary.Medium, fontSize: normalizeFont(14), color: !selected ? '#0E134F' : '#FFFFFF' }}>List Engine</Text>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                        <Text style={{ color: '#42526E', fontFamily: fontFamily.Primary.Medium, fontSize: normalizeFont(14), color: !selected ? '#0E134F' : '#FFFFFF' }}>Document Storage</Text>
                    </View>
                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center', marginTop: normalizeHeight(20) }} >
                        {
                            selected ?
                                <CustomButton buttonColor="white" buttonText={"Buy Now"} />
                                :
                                <CustomButton buttonTextColor="white" buttonText={"Buy Now"} />
                        }
                        {/* <CustomButton buttonTextColor="white" buttonText={"AQUA SHOWS"} /> */}
                    </View>
                </View>
            </View>
        )
    }
    // {SubscriptionBox()}
    return (
        <ViewHeader>
            <BackHeader heading={'Subscription'} />
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around' }}>
                {
                    [{ planName: 'Starter', amount: 15, duration: 'Month', selected: false }, { planName: 'Starter', amount: 15, duration: 'Month', selected: true }, { planName: 'Starter', amount: 15, duration: 'Month', selected: false }].map((eachData, index) => {
                        return (
                            <React.Fragment key={index}>
                                {SubscriptionBox({ ...eachData })}
                            </React.Fragment>
                        )
                    })
                }
            </View>

        </ViewHeader>
    )
}
export default SubscriptionScreen;