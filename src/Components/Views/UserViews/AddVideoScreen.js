import React, { useState } from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { OrangeCheckIcon } from '../../../assets/images'
import { containerShadow, fontFamily, fontH2, fontH3, textPrimayColor } from '../../../Constants/styles'
import { normalizeHeight, normalizeWidth, normalizeWithScale } from '../../../Utils/fontUtil'
import { handleValidateSingleField, validationFieldsKeys } from '../../../Utils/validationUtil'
import CustomBoxInput from '../../SharedComponents/BoxInput'
import AddPlaceHolder from '../Shared/AddPlaceHolder'
import BackHeader from '../Shared/BackHeader'
import ViewHeader from '../Shared/ViewHeader'

const AddVideoScreen = (props) => {
    const [addVideoForm, setAddVideoForm] = useState({
        title: { keyName: "title", value: '', error: null, vaidateOption: new validationFieldsKeys({ fieldName: "title", Min: 0, Max: 50 }) },
        date: { keyName: "date", value: '', error: null, vaidateOption: new validationFieldsKeys({ fieldName: "date", Min: 0, Max: 50 }) },
    })
    const handleOnValueChange = (value, FormKeyName) => {
        let formObject = { ...addVideoForm };
        formObject[`${FormKeyName}`] = { ...formObject[`${FormKeyName}`], value: value, error: null };
        setAddVideoForm(formObject);
    }
    const onBlur = (attrName) => {
        let isValid = handleValidateSingleField(addVideoForm, attrName);
        if (isValid.IsError) {
            let formObject = { ...addVideoForm };
            formObject[`${attrName}`] = { ...formObject[`${attrName}`], error: isValid.FieldError };
            setAddVideoForm(formObject);
        }
    }
    return (
        <ViewHeader>
            <BackHeader heading={'Add Video'} />
            <View style={{ flex: 1 }}>
                <View style={styles.container}>
                    <View style={{ flex: 1.2, }}>
                        <View style={{ padding: normalizeWidth(10) }}>
                            <Text style={{ fontFamily: fontFamily.Primary.Medium, color: textPrimayColor, fontSize: fontH3 }}>Add Horse</Text>
                            <View style={{ marginTop: normalizeHeight(20) }}>
                                <AddPlaceHolder customStyles={{ width: normalizeWidth(100), height: normalizeWidth(50), borderRadius: normalizeWidth(2) }} />
                            </View>
                        </View>
                    </View>
                    <View style={{ flex: 1.5, }}>
                        <View style={{ flex: 1, justifyContent: 'center' }}>
                            <CustomBoxInput
                                attrName={addVideoForm.title.keyName}
                                title={"Name"}
                                Heading="Video Title"
                                value={addVideoForm.title.value}
                                updateMasterState={handleOnValueChange}
                                onBlur={onBlur}
                                error={addVideoForm.title.error}
                                isRequired={false}
                                simpleInput={true}
                            />
                        </View>
                        <View style={{ flex: 1, justifyContent: 'center' }}>
                            <CustomBoxInput
                                attrName={addVideoForm.date.keyName}
                                title={"dd/mm/yy"}
                                Heading="Date"
                                value={addVideoForm.date.value}
                                updateMasterState={handleOnValueChange}
                                onBlur={onBlur}
                                error={addVideoForm.date.error}
                                isRequired={false}
                            />
                        </View>
                    </View>
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end', }}>
                    <Image source={OrangeCheckIcon} style={{ width: normalizeWidth(25), height: normalizeWidth(25), resizeMode: 'contain', marginRight: normalizeWidth(10), marginBottom: normalizeWidth(10) }} />
                </View>
            </View>
        </ViewHeader>
    )
}

export default AddVideoScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        ...containerShadow,
        padding: normalizeWidth(15),
        backgroundColor: '#FFFFFF',
        marginBottom: normalizeHeight(40),
        borderRadius: normalizeWidth(2),
        flexDirection: 'row'
    }
})
