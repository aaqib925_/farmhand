import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { containerShadow, fontFamily, fontH2, fontH3, SettingsContainerHeadingStyle, SettingsContainerStyles } from '../../../../Constants/styles'
import { normalizeHeight, normalizeWidth } from '../../../../Utils/fontUtil'

const EmailSettings = () => {
    return (
        <View style={{ ...SettingsContainerStyles }}>
            <Text style={{ ...SettingsContainerHeadingStyle }}>Email Settings</Text>
        </View>
    )
}

export default EmailSettings

const styles = StyleSheet.create({})
