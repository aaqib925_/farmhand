import React from 'react'
import { Image, Pressable, StyleSheet, Text, View } from 'react-native'
import { CheckBoxActive, CheckBoxInActive, NextColoredIcon } from '../../../../assets/images'
import { fontFamily, fontH2V3, textPrimayColor } from '../../../../Constants/styles'
import { normalizeHeight, normalizeWidth } from '../../../../Utils/fontUtil'

const LanguageListView = ({ title, isLast, currentItem, id, onPress }) => {
    return (
        <Pressable onPress={() => onPress(id)} style={{ flex: 1, paddingVertical: normalizeHeight(20), borderBottomWidth: isLast ? 0 : StyleSheet.hairlineWidth, borderBottomColor: '#42526E33' }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <Text style={{ fontFamily: currentItem === id ? fontFamily.Primary.Medium : fontFamily.Primary.Regular, fontSize: fontH2V3, color: currentItem === id ? textPrimayColor : 'black' }}>{title}</Text>
                <Image source={currentItem === id ? CheckBoxActive : CheckBoxInActive} style={{ width: normalizeWidth(5), height: normalizeWidth(5), resizeMode: "contain" }} />
            </View>
        </Pressable>
    )
}

export default LanguageListView

const styles = StyleSheet.create({})
