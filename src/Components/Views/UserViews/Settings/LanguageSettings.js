import React, { useState } from 'react'
import { FlatList, StyleSheet, Text, View } from 'react-native'
import { languageData } from '../../../../Constants/enum'
import { containerShadow, SettingsContainerHeadingStyle, SettingsContainerStyles } from '../../../../Constants/styles'
import { normalizeHeight, normalizeWidth } from '../../../../Utils/fontUtil'
import LanguageListView from './LanguageListView'

const LanguageSettings = () => {
    const [currentItem, setCurrentItem] = useState(languageData[0].id)
    const renderItem = ({ index, item }) => {
        return (
            <LanguageListView onPress={onPressLanguageItem} currentItem={currentItem} title={item.name} id={item.id} isLast={index + 1 === languageData.length ? true : false} />
        )
    }
    const onPressLanguageItem = (itemId) => {
        setCurrentItem(itemId)
    }
    return (
        <View style={{ ...SettingsContainerStyles }}>
            <Text style={{ ...SettingsContainerHeadingStyle, marginBottom: normalizeHeight(20) }}>Change Language</Text>
            <FlatList
                data={languageData}
                keyExtractor={(item, index) => item.id}
                renderItem={renderItem}
            />
        </View>
    )
}

export default LanguageSettings

const styles = StyleSheet.create({})
