import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { containerShadow, SettingsContainerHeadingStyle, SettingsContainerStyles } from '../../../../Constants/styles'
import { normalizeWidth } from '../../../../Utils/fontUtil'

const PhoneSettings = () => {
    return (
        <View style={{ ...SettingsContainerStyles }}>
            <Text style={{ ...SettingsContainerHeadingStyle }}>Phone Settings</Text>
        </View >
    )
}

export default PhoneSettings

const styles = StyleSheet.create({})
