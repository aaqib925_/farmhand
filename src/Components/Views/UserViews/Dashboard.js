import React, { useState } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList, Pressable } from 'react-native'
import { AwayCareImage, BreedingRecordsImage, cross, DashboardIconImage, HealthRemainderImage, HorseImage, LogoWithTextImage, MedicalRecordsImage, RacingImage, VetChatImage } from '../../../assets/images'
import { RouteNames } from '../../../Constants/routeNames'
import { fontFamily, fontH2, fontH2V2, fontH2V3, fontH3, textPrimayColor, textSecondaryColor } from '../../../Constants/styles'
import { normalizeFont, normalizeHeight, normalizeWidth } from '../../../Utils/fontUtil'
import MenuHeader from "../Shared/MenuHeader"
import ViewHeader from '../Shared/ViewHeader'
import ImageContainer from "../../SharedComponents/ImageContainer"
import { menuProfile } from '../../../Constants/enum'
const Dashboard = ({ navigation, ...props }) => {
    const headerMenuFunction = props?.route?.params?.toggleMenuDrawer;
    const toggleNotificationDrawer = props?.route?.params?.toggleNotificationDrawer
    const BoxData = [{
        image: DashboardIconImage,
        text: 'Dashboard',
        onPress: () => navigation.navigate(RouteNames.User.AnimalType)
    }, {
        image: AwayCareImage,
        text: 'Away Care'
    }, {
        image: VetChatImage,
        text: 'Vet Chat'
    }, {
        image: HealthRemainderImage,
        text: 'Health Remainder'
    }, {
        image: BreedingRecordsImage,
        text: 'Breeding Records'
    }, {
        image: MedicalRecordsImage,
        text: 'Medical Records'
    }, {
        image: RacingImage,
        text: 'Racing'
    }];
    const renderBox = ({ item }) => {
        const { image, text, onPress } = item;
        const handleOnPress = () => {
            if (onPress) {
                onPress();
            }
        }
        return (
            <TouchableOpacity onPress={handleOnPress} style={{ backgroundColor: '#15689C', borderRadius: normalizeWidth(5), justifyContent: 'center', alignItems: 'center', paddingVertical: normalizeHeight(25), width: '30%' }}>

                <View style={{ marginBottom: normalizeHeight(20) }}>
                    <Image style={{ width: normalizeWidth(50), height: normalizeHeight(50) }} source={image} resizeMode={'contain'}></Image>
                </View>
                <View >
                    <Text style={{ fontFamily: fontFamily.Primary.Medium, fontSize: normalizeFont(12), color: '#FFFFFF', textAlign: 'center' }}>{text}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    return (

        <ViewHeader headerStyle={{ backgroundColor: '#2080BD' }} >
            <MenuHeader headerMenuFunction={headerMenuFunction} toggleNotificationDrawer={toggleNotificationDrawer} />
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                <Image style={{ width: normalizeWidth(45), height: normalizeHeight(80) }} resizeMode={'contain'} source={LogoWithTextImage}></Image>
                <View style={{ marginLeft: normalizeWidth(5), justifyContent: 'space-around' }}>
                    <Text style={{ fontFamily: fontFamily.Primary.Bold, fontSize: normalizeFont(20), color: '#FFFFFF' }}>Taylor Lockwood</Text>
                    <Text style={{ fontFamily: fontFamily.Primary.Regular, opacity: 0.8, fontSize: normalizeFont(14), color: '#FFFFFF' }}>taylorlookwood @gmail.com</Text>
                </View>
            </View>

            <View style={{ marginTop: normalizeHeight(40) }}>
                <FlatList
                    data={BoxData}
                    renderItem={renderBox}
                    columnWrapperStyle={{ flexDirection: 'row', justifyContent: 'space-around', marginBottom: normalizeHeight(30), flex: 1 }}
                    numColumns={3}
                />
            </View>

        </ViewHeader>
    )
}

export default Dashboard

