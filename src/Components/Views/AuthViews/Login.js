import React from 'react'
import { ImageBackground, StyleSheet, Text, View } from 'react-native'
import { fontFamily } from '../../../Constants/styles'
import { BackgroundImage } from "../../../assets/images/index"
import { normalizeWidth } from '../../../Utils/fontUtil'
import LoginTabs from '../../Navigators/LoginNavigator'
const Login = () => {
    return (
        <ImageBackground resizeMode="cover" source={BackgroundImage} style={styles.background}>
            <View style={{ flex: 1 }}>
            </View>
            <View style={styles.secondContainer}>
                <View style={styles.card}>
                    <LoginTabs />
                </View>
            </View>
        </ImageBackground>
    )
}

export default Login

const styles = StyleSheet.create({
    background: {
        width: '100%',
        height: "100%",
        flexDirection: 'row'
    },
    secondContainer: {
        flex: 1.2,
        paddingVertical: normalizeWidth(10),
        paddingHorizontal: normalizeWidth(12)
    },
    card: {
        flex: 1,
        backgroundColor: 'white',
        borderRadius: normalizeWidth(7),
        paddingVertical: normalizeWidth(10),
    }
})
