import React, { useState } from 'react'
import { StyleSheet, Text, View, Pressable, Image } from 'react-native'
import { ButtonNextIcon, EmailIcon, LockIcon, user } from '../../../../assets/images'
import { RouteNames } from '../../../../Constants/routeNames'
import { fontFamily, fontH2, fontH3, fontH3V3, secondaryColor } from '../../../../Constants/styles'
import { normalizeFont, normalizeHeight, normalizeWidth } from '../../../../Utils/fontUtil'
import { handleValidateSingleField, validationFieldsKeys } from '../../../../Utils/validationUtil'
import CustomTextInput from '../../../SharedComponents/CustomTextInput'
import CheckBox from '@react-native-community/checkbox';

const SignupTab = ({ navigation }) => {
    const [isSelected, setSelection] = useState(false);
    const [registerForm, setRegisterForm] = useState({
        name: { keyName: "name", value: '', error: null, vaidateOption: new validationFieldsKeys({ fieldName: "name", Min: 0, Max: 50, isEmail: true }) },
        emailAddress: { keyName: "emailAddress", value: '', error: null, vaidateOption: new validationFieldsKeys({ fieldName: "emailAddress", Min: 0, Max: 50, isEmail: true }) },
        password: { keyName: "password", value: '', error: null, vaidateOption: new validationFieldsKeys({ fieldName: "password", Min: 0, Max: 50 }) },
    })
    const handleOnValueChange = (value, FormKeyName) => {
        let formObject = { ...registerForm };
        formObject[`${FormKeyName}`] = { ...formObject[`${FormKeyName}`], value: value, error: null };
        setRegisterForm(formObject);
    }
    const onBlur = (attrName) => {
        let isValid = handleValidateSingleField(registerForm, attrName);
        if (isValid.IsError) {
            let formObject = { ...registerForm };
            formObject[`${attrName}`] = { ...formObject[`${attrName}`], error: isValid.FieldError };
            setRegisterForm(formObject);
        }
    }
    const handleRedirection = (routeNames, param) => {
        console.log("REDIRECT")
    }
    return (
        <View style={styles.container}>
            <View style={{ flex: 1.4, justifyContent: 'flex-end' }}>
                <Text style={{ fontFamily: fontFamily.Primary.Heavy, fontSize: normalizeFont(25), color: '#0E134F', marginBottom: normalizeHeight(5) }}>Welcome To Farm Hand</Text>
                <Text style={{ fontFamily: fontFamily.Primary.Regular, fontSize: normalizeFont(13), color: '#42526E', paddingRight: normalizeWidth(5) }}>Please enter all the details to explore a great experience</Text>
            </View>
            <View style={{ flex: 2, justifyContent: 'center' }}>
                <CustomTextInput
                    attrName={registerForm.name.keyName}
                    title={"Email"}
                    value={registerForm.name.value}
                    updateMasterState={handleOnValueChange}
                    onBlur={onBlur}
                    error={registerForm.name.error}
                    isRequired={false}
                    simpleInput={true}
                    leftIcon={EmailIcon}
                    keyboardType={"email-address"}
                />
                <CustomTextInput
                    attrName={registerForm.emailAddress.keyName}
                    title={"Name"}
                    value={registerForm.emailAddress.value}
                    updateMasterState={handleOnValueChange}
                    onBlur={onBlur}
                    error={registerForm.emailAddress.error}
                    isRequired={false}
                    simpleInput={true}
                    leftIcon={user}
                    keyboardType={"email-address"}
                />
                <CustomTextInput
                    attrName={registerForm.password.keyName}
                    title={"Password"}
                    value={registerForm.password.value}
                    updateMasterState={handleOnValueChange}
                    onBlur={onBlur}
                    error={registerForm.password.error}
                    isRequired={false}
                    simpleInput={true}
                    leftIcon={LockIcon}
                    showEye={true}
                />
                <View style={{ flexDirection: 'row', marginTop: normalizeHeight(10), alignItems: 'center' }}>
                    <CheckBox
                        boxType="square"
                        value={isSelected}
                        onValueChange={setSelection}
                        style={{ marginHorizontal: 2 }}
                    />
                    <Text style={{ fontFamily: fontFamily.Primary.Regular, fontSize: fontH3V3, marginLeft: normalizeWidth(2) }}>I agree to all</Text>
                    <Pressable onPress={() => handleRedirection(RouteNames.AuthRoutes.SignUp)}>
                        <Text style={{ fontFamily: fontFamily.Primary.Regular, textDecorationLine: 'underline', color: secondaryColor, fontSize: fontH3V3 }}>{" terms & conditions"}</Text>
                    </Pressable>
                </View>

            </View>
            <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                <Image style={styles.nextButton} source={ButtonNextIcon} resizeMode='contain' />
            </View>
        </View>
    )
}

export default SignupTab

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        paddingBottom: normalizeHeight(20),
        paddingHorizontal: normalizeWidth(15)
    },
    nextButton: {
        width: normalizeWidth(20),
        height: normalizeWidth(20)
    }
})
