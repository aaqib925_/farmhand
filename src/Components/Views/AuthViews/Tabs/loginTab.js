import React, { useState } from 'react'
import { StyleSheet, Text, View, Pressable, Image, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { ButtonNextIcon, EmailIcon } from '../../../../assets/images'
import { RouteNames } from '../../../../Constants/routeNames'
import { fontFamily, fontH2, fontH3, secondaryColor } from '../../../../Constants/styles'
import { normalizeFont, normalizeHeight, normalizeWidth } from '../../../../Utils/fontUtil'
import { handleValidateSingleField, validationFieldsKeys } from '../../../../Utils/validationUtil'
import CustomTextInput from '../../../SharedComponents/CustomTextInput'
import { login } from "../../../../Redux/Actions/index"
const LoginTab = ({ navigation, login }) => {
    const [loginForm, setLoginForm] = useState({
        emailAddress: { keyName: "emailAddress", value: '', error: null, vaidateOption: new validationFieldsKeys({ fieldName: "emailAddress", Min: 0, Max: 50, isEmail: true }) },
        password: { keyName: "password", value: '', error: null, vaidateOption: new validationFieldsKeys({ fieldName: "password", Min: 0, Max: 50 }) },
    })
    const handleOnValueChange = (value, FormKeyName) => {
        let formObject = { ...loginForm };
        formObject[`${FormKeyName}`] = { ...formObject[`${FormKeyName}`], value: value, error: null };
        setLoginForm(formObject);
    }
    const onBlur = (attrName) => {
        let isValid = handleValidateSingleField(loginForm, attrName);
        if (isValid.IsError) {
            let formObject = { ...loginForm };
            formObject[`${attrName}`] = { ...formObject[`${attrName}`], error: isValid.FieldError };
            setLoginForm(formObject);
        }
    }
    const handleRedirection = (routeNames, param) => {
  
        console.log("REDIRECT")
    }
    const handleOnSubmit =  () => {
        console.log("ON SUBMIT",login)
        login();
    }
    return (
        <View style={styles.container}>
            <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                <Text style={{ fontFamily: fontFamily.Primary.Heavy, fontSize: normalizeFont(25), color: '#0E134F', marginBottom: normalizeHeight(5) }}>Welcome Back</Text>
                <Text style={{ fontFamily: fontFamily.Primary.Regular, fontSize: normalizeFont(13), color: '#42526E' }}>Please enter your credentials to Login</Text>
            </View>
            <View style={{ flex: 2, justifyContent: 'center', marginBottom: normalizeHeight(10) }}>
                <CustomTextInput
                    attrName={loginForm.emailAddress.keyName}
                    title={"Email"}
                    value={loginForm.emailAddress.value}
                    updateMasterState={handleOnValueChange}
                    onBlur={onBlur}
                    error={loginForm.emailAddress.error}
                    isRequired={false}
                    simpleInput={true}
                    leftIcon={EmailIcon}
                    keyboardType={"email-address"}
                />
                <CustomTextInput
                    attrName={loginForm.password.keyName}
                    title={"Password"}
                    value={loginForm.password.value}
                    updateMasterState={handleOnValueChange}
                    onBlur={onBlur}
                    error={loginForm.password.error}
                    isRequired={false}
                    simpleInput={true}
                    leftIcon={EmailIcon}
                    showEye={true}
                />
                <Pressable onPress={() => handleRedirection(RouteNames.AuthRoutes.SignUp)} style={{ alignItems: 'flex-end', marginTop: normalizeHeight(5) }}>
                    <Text style={{ fontFamily: fontFamily.Primary.Medium, textDecorationLine: 'underline', color: secondaryColor, fontSize: fontH3 }}>Forgot password?</Text>
                </Pressable>
            </View>
            <TouchableOpacity onPress={handleOnSubmit} style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                <Image style={styles.nextButton} source={ButtonNextIcon} resizeMode='contain' />
            </TouchableOpacity>
        </View>
    )
}
const mapDispatchToProps = {
    login
}
export default connect(null, mapDispatchToProps)(LoginTab)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        paddingBottom: normalizeHeight(20),
        paddingHorizontal: normalizeWidth(15)
    },
    nextButton: {
        width: normalizeWidth(20),
        height: normalizeWidth(20)
    }
})
