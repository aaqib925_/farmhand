import React from "react"
import { View, Text, Image, StyleSheet,TouchableOpacity } from "react-native"
import { TickImage } from "../../../assets/images";
import { fontFamily } from "../../../Constants/styles";
import { normalizeFont, normalizeHeight, normalizeWidth } from "../../../Utils/fontUtil";
const AnimalBox = ({ id,name, image_url, showSelection,onPress,selectedId }) => {
    const handlePress = () => {
        if(onPress) {
            onPress(id);
        }
    }
    return (
        <TouchableOpacity onPress={handlePress} style={{ ...styles.boxShadow, width: '25%', backgroundColor: '#FFFFFF', borderRadius: normalizeWidth(2), padding: normalizeWidth(2) }}>
            {
                showSelection ?
                    <View style={{ justifyContent :'center',alignItems :'center' ,width: normalizeWidth(6), height: normalizeWidth(6), borderRadius: (normalizeWidth(6) / 2),backgroundColor : selectedId === id ? '#2080BD' : '#FFFFFF', borderColor: selectedId === id ? '#2080BD'  : '#42526E30', borderWidth: 1, position: 'absolute', marginTop: normalizeHeight(8), marginLeft: normalizeHeight(8) }} >
                        <Image source={TickImage} resizeMode={'contain'} />
                    </View> : <></>
            }
            <View style={{ justifyContent: 'center', alignItems: 'center', paddingHorizontal: normalizeWidth(1), paddingVertical: normalizeWidth(5) }}>
                <Image style={{ width: normalizeWidth(30), height: normalizeWidth(30) }} source={image_url} resizeMode={'contain'}></Image>
            </View>
            <View style={{ marginBottom: normalizeHeight(5) }}>
                <Text style={{ fontFamily: fontFamily.Primary.Medium, fontSize: normalizeFont(13), color: '#0E134F', textAlign: 'center' }}>{name}</Text>
            </View>
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    boxShadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    }
})
export default AnimalBox;