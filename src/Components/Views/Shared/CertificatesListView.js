import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { normalizeWidth } from '../../../Utils/fontUtil'

const CertificatesListView = ({ item, width = 90 }) => {
    return (
        <View style={{ marginRight: normalizeWidth(2), width: normalizeWidth(width), borderRadius: normalizeWidth(3), borderWidth: StyleSheet.hairlineWidth, borderColor: "#42526E30", overflow: 'hidden', justifyContent: 'center', alignItems: 'center' }}>
            <Image source={item.image} style={{ width: "100%", height: '100%' }} resizeMode="contain" />
        </View>
    )
}

export default CertificatesListView

const styles = StyleSheet.create({})
