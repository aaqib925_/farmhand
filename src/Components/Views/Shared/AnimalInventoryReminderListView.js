import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { fontFamily, fontH2V3, fontH3, fontH3V3, textPrimayColor, textSecondaryColor } from '../../../Constants/styles'
import { normalizeFont, normalizeHeight } from '../../../Utils/fontUtil'

const AnimalInventoryReminderListView = ({ item }) => {
    return (
        <View style={{ flex: 1, paddingVertical: normalizeHeight(20), borderBottomWidth: StyleSheet.hairlineWidth, borderBottomColor: textSecondaryColor }}>
            <Text style={{ fontFamily: fontFamily.Primary.Heavy, fontSize: fontH3V3, color: textPrimayColor }}>{item.name}</Text>
            <Text style={{ fontFamily: fontFamily.Primary.Regular, fontSize: normalizeFont(8), color: textSecondaryColor }}>{item.time}</Text>
        </View>
    )
}

export default AnimalInventoryReminderListView

const styles = StyleSheet.create({

})
