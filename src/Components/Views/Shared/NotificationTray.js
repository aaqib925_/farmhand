import React from "react"
import { View, Image, StyleSheet, Text } from "react-native"
import { annoucementImage, BackColoredIcon, NextColoredIcon } from "../../../assets/images";
import { fontFamily } from "../../../Constants/styles";
import { normalizeHeight, normalizeWidth } from "../../../Utils/fontUtil";

const NotificationTray = () => {
    return (
        <View style={{ backgroundColor: '#2080BD10', flexDirection: 'row', padding: normalizeWidth(4), justifyContent: 'space-between' }}>
            <View style={{ flexDirection: 'row' }}>
                <Image style={{ width: normalizeWidth(10), height: normalizeHeight(20) }} source={annoucementImage} />
                <View style={{ width: StyleSheet.hairlineWidth, backgroundColor: '#2080BD', marginLeft: normalizeWidth(10), marginRight: normalizeWidth(10) }}></View>
                <Text style={{ color: '#2080BD', fontFamily: fontFamily.Primary.Bold, marginRight: normalizeWidth(2), textAlign: 'center', alignItems: 'center' }}>May 01, 2021</Text>
                <Text style={{ color: '#2080BD90', fontFamily: fontFamily.Primary.Regular, textAlign: 'center', alignItems: 'center' }}>Medina Spirit wins Kentucky Derby</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
                <Image style={{ width: normalizeWidth(10), height: normalizeHeight(20), marginRight: normalizeWidth(5) }} source={BackColoredIcon} />
                <Image style={{ width: normalizeWidth(10), height: normalizeHeight(20) }} source={NextColoredIcon} />
            </View>
        </View>
    )
}
export default NotificationTray;