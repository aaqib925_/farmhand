import { useNavigation } from "@react-navigation/core"
import React from "react"
import { View, Image, Text, Pressable } from "react-native"
import { BackColoredIcon, NotificationColoredIcon, HeaderPlusColoredIcon, backButtonWhite, HeaderPlusColoredIconWhite, bellWhite } from "../../../assets/images/index"
import { fontFamily } from "../../../Constants/styles"
import { normalizeFont, normalizeHeight, normalizeWidth } from "../../../Utils/fontUtil"
const BackHeader = ({ heading, useWhiteTheme = false }) => {
    const navigation = useNavigation();
    const onPressBack = () => {
        navigation.goBack();
    }
    return (
        <View style={{ flexDirection: 'row', marginBottom: normalizeHeight(40), justifyContent: 'space-between', alignItems: 'center' }}>
            <Pressable onPress={onPressBack}>
                <Image resizeMode={'contain'} style={{ width: normalizeWidth(10), height: normalizeHeight(20) }} source={useWhiteTheme ? backButtonWhite : BackColoredIcon} />
            </Pressable>
            {heading ?
                <Text style={{ color: useWhiteTheme ? "white" : "#0E134F", fontFamily: fontFamily.Primary.Bold, fontSize: normalizeFont(18) }}>{heading}</Text> : <></>
            }
            <View style={{ flexDirection: 'row' }}>
                <Image resizeMode={'contain'} style={{ width: normalizeWidth(10), height: normalizeHeight(20), marginRight: normalizeWidth(5) }} source={useWhiteTheme ? HeaderPlusColoredIconWhite : HeaderPlusColoredIcon} />
                <Image resizeMode={'contain'} style={{ width: normalizeWidth(10), height: normalizeHeight(20) }} source={useWhiteTheme ? bellWhite : NotificationColoredIcon} />
            </View>
        </View>
    )
}

export default BackHeader