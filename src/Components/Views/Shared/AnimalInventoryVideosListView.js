import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { HorseImage, HorseVideoIcon, PlayIcon } from '../../../assets/images'
import { fontFamily, fontH2, fontH3, fontH3V3, textPrimayColor } from '../../../Constants/styles'
import { normalizeHeight, normalizeWidth } from '../../../Utils/fontUtil'

const AnimalInventoryVideosListView = ({ item }) => {
    return (
        <View>
            <View style={{ justifyContent: 'center', alignItems: 'center', width: normalizeWidth(60), height: normalizeWidth(38), marginRight: normalizeWidth(5), overflow: 'hidden', borderRadius: normalizeWidth(2), }}>
                <Image source={PlayIcon} style={{ zIndex: 99, position: 'absolute', width: normalizeWidth(10), height: normalizeWidth(10), marginBottom: normalizeHeight(20) }} resizeMode="contain" />
                <Image source={HorseVideoIcon} resizeMode="contain" style={{ width: '100%', height: '100%' }} />
            </View>
            <Text style={{ fontFamily: fontFamily.Primary.Heavy, fontSize: fontH3, color: textPrimayColor }}>{item.title}</Text>
            <Text style={{ fontSize: fontH3V3, color: "#42526EB2", fontFamily: fontFamily.Primary.Regular }}>{item.date}</Text>
        </View>
    )
}

export default AnimalInventoryVideosListView

const styles = StyleSheet.create({})
