import React from "react"
import { View } from "react-native"
import { normalizeWidth } from "../../../Utils/fontUtil";
const ViewHeader = ({ children, headerStyle }) => {
    return (
        <View style={{  flex: 1, padding: normalizeWidth(10), ...headerStyle }}>
            {children}
        </View>
    )
}
export default ViewHeader;