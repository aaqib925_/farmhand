import React from "react"
import { View, Image, Text } from "react-native"
import { fontFamily } from "../../../Constants/styles";
import { normalizeFont, normalizeWidth, normalizeHeight } from "../../../Utils/fontUtil";
const CountBox = ({ count = 10, name = 'Total Horse', backgroundColor = '#2080BD' }) => {
    return (
        <View style={{ width: '15%', backgroundColor: backgroundColor, borderRadius: normalizeWidth(2),paddingVertical : normalizeHeight(30), justifyContent: 'center', alignItems: 'center' }}>
            <View style={{ marginBottom: normalizeHeight(5) }}>
                <Text style={{ fontFamily: fontFamily.Primary.Bold, fontSize: normalizeFont(24), color: '#FFFFFF', textAlign: 'center' }}>{count}</Text>
            </View>
            <View style={{ marginBottom: normalizeHeight(5) }}>
                <Text style={{ fontFamily: fontFamily.Primary.Medium, fontSize: normalizeFont(13), color: '#FFFFFF', textAlign: 'center' }}>{name}</Text>
            </View>
        </View>
    )
}
export default CountBox;