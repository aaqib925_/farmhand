import { useNavigation } from "@react-navigation/core"
import React from "react"
import { View, Image, Text, Pressable } from "react-native"
import { MenuIcon, NotificationWhiteIconImage, HeaderPlusIconImage } from "../../../assets/images/index"
import { normalizeHeight, normalizeWidth } from "../../../Utils/fontUtil"
const MenuHeader = ({ headerMenuFunction, toggleNotificationDrawer }) => {
    console.log("REACHED***********", toggleNotificationDrawer)

    return (
        <View style={{ flexDirection: 'row', marginBottom: normalizeHeight(40), justifyContent: 'space-between' }}>
            <Pressable onPress={headerMenuFunction}>
                <Image resizeMode={'contain'} style={{ width: normalizeWidth(10), height: normalizeHeight(20) }} source={MenuIcon} />
            </Pressable>
            <View style={{ flexDirection: 'row' }}>
                <Image resizeMode={'contain'} style={{ width: normalizeWidth(10), height: normalizeHeight(20), marginRight: normalizeWidth(5) }} source={HeaderPlusIconImage} />
                <Pressable onPress={toggleNotificationDrawer}>
                    <Image resizeMode={'contain'} style={{ width: normalizeWidth(10), height: normalizeHeight(20) }} source={NotificationWhiteIconImage} />
                </Pressable>

            </View>
        </View>
    )
}

export default MenuHeader