import React from "react"
import { View, Image, Pressable } from "react-native"
import { addPlusPlaceHolder } from "../../../assets/images"
import { normalizeWidth } from "../../../Utils/fontUtil"

const AddPlaceHolder = ({ customStyles, imageSize = 30, onPress }) => {
    return (
        <Pressable onPress={onPress}>
            <View style={{
                width: '22%',
                backgroundColor: '#E9F3F9',
                borderWidth: 1,
                borderStyle: 'dashed',
                borderRadius: 1,
                borderColor: '#2080BD',
                justifyContent: 'center',
                alignItems: 'center',
                ...customStyles
            }}>
                <Image style={{ width: normalizeWidth(imageSize), height: normalizeWidth(imageSize) }} source={addPlusPlaceHolder} resizeMode={'contain'} />
            </View>
        </Pressable>
    )
}
export default AddPlaceHolder