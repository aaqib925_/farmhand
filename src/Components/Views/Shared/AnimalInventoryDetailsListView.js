import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { fontFamily, fontH2, fontH2V2, fontH3 } from '../../../Constants/styles'
import { normalizeWidth } from '../../../Utils/fontUtil'

const AnimalInventoryDetailsListView = ({ item }) => {
    return (
        <View style={{ flex: 1, marginRight: normalizeWidth(2), }}>
            <Text style={styles.title}>{item.title}</Text>
            <Text style={styles.value} >{item.value}</Text>
        </View>
    )
}

export default AnimalInventoryDetailsListView

const styles = StyleSheet.create({
    title: {
        fontFamily: fontFamily.Primary.Medium,
        color: '#42526EB2',
        fontSize: fontH3
    },
    value: {
        fontFamily: fontFamily.Primary.Medium,
        color: '#0E134F',
        fontSize: fontH3
    }
})
