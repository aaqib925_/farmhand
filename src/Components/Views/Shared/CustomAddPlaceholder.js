import React from "react"
import { View, Image } from "react-native"
import { addPlusPlaceHolder } from "../../../assets/images"
import { normalizeWidth } from "../../../Utils/fontUtil"

const CustomAddPlaceholder = ({ width = 60, marginRight = 5, height = 40 }) => {
    return (
        <>
            <View style={{
                width: normalizeWidth(width),
                height: normalizeWidth(height),
                backgroundColor: '#E9F3F9',
                borderWidth: 1,
                borderStyle: 'dashed',
                borderRadius: 1,
                borderColor: '#2080BD',
                justifyContent: 'center',
                alignItems: 'center',
                marginRight: normalizeWidth(marginRight)
            }}>
                <Image style={{ width: normalizeWidth(30), height: normalizeWidth(30) }} source={addPlusPlaceHolder} resizeMode={'contain'} />
            </View>
        </>
    )
}
export default CustomAddPlaceholder