import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import { primaryColor } from '../../Constants/styles';
const Loading = ({ size = "small", color = primaryColor }) => {
    return (
        <View>
            <ActivityIndicator size={size} color={color} />
        </View>
    )
}

export default Loading