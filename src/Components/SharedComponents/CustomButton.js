import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
    ActivityIndicator,
    ImageBackground
} from 'react-native';
import { buttonPrimary, defaultButtonTextColor, fontFamily, fontH2, fontH2V3, fontH3, fontH3V3, primaryColor, shadowColors } from '../../Constants/styles';
import { normalizeHeight, normalizeWidth } from '../../Utils/fontUtil';
const CustomButton = ({ buttonColor = buttonPrimary, buttonText, onPress, ImgSource, disabled, loading = false, activityIndicatorColor = primaryColor, containerPadding = 15, buttonTextColor = buttonPrimary, fontSize = fontH3V3, borderRadius = normalizeWidth(2), capital = false, Line = false, customStyles = {}, customTextStyles = {} }) => {
    const onButtonPress = () => {
        onPress()
    }
    return (
        <TouchableOpacity onPress={onButtonPress} disabled={disabled}>
            <View
                resizeMode="contain"
                style={{
                    ...styles.buttonContainer,
                    borderRadius: borderRadius,
                    backgroundColor: buttonColor,
                    opacity: disabled ? 0.3 : 1,
                    borderWidth: StyleSheet.hairlineWidth,
                    borderColor: buttonPrimary,
                    marginBottom: normalizeWidth(5),
                    paddingVertical: normalizeHeight(8),
                    ...customStyles,
                }}
            >
                {
                    ImgSource && !loading ?
                        <View style={{ width: '20%', alignItems: 'center' }}>
                            <Image source={ImgSource} style={{ width: normalizeWidth(15), height: normalizeWidth(15) }} resizeMode={"contain"} />
                        </View>
                        : <></>
                }
                {
                    Line && !loading ?
                        <View style={{ width: '10%', height: normalizeHeight(20) }}>
                            <View style={styles.line}>
                            </View>
                        </View>
                        : <></>
                }
                {!loading ?
                    <View style={{ width: ImgSource ? '70%' : '100%', alignItems: ImgSource ? 'flex-start' : "center" }}><Text numberOfLines={1} style={{ ...styles.buttonText, color: buttonTextColor, fontSize: fontSize, ...customTextStyles }}>{capital ? buttonText.toUpperCase() : buttonText}</Text></View> : <View style={{ paddingRight: normalizeWidth(10) }}>
                        <ActivityIndicator color={activityIndicatorColor} />
                    </View>}


            </View>
        </TouchableOpacity>
    )
}
export default CustomButton
const styles = StyleSheet.create({
    buttonContainer: {
        width: "100%",
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    buttonText: {
        fontFamily: fontFamily.Primary.Medium
    },
    iconImage: {
        height: normalizeHeight(35),
        width: normalizeWidth(30)
    },
    line: {
        height: '100%',
        width: 1,
        backgroundColor: primaryColor,
    },

})