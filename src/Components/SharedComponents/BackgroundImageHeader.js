import React, { Children } from "react"
import { View, StyleSheet, ImageBackground, Text, Image, FlatList } from "react-native"
import { HorseHeaderBackgroundImage, HorseImage, HorseVideoIcon, OrangePlusIcon } from "../../assets/images"
import { containerShadow, fontFamily, fontH2, fontH3, textPrimayColor } from "../../Constants/styles"
import { normalizeHeight, normalizeWidth, normalizeWithScale } from "../../Utils/fontUtil"
import BackHeader from "../Views/Shared/BackHeader"

const BackgroundImageHeader = ({ children, heading = "Heading", useWhiteTheme = true }) => {

    return (
        <View style={{ flex: 1, }}>
            <View style={{ flex: 1, borderBottomRightRadius: normalizeWidth(30), borderBottomLeftRadius: normalizeWidth(30), overflow: 'hidden' }}>
                <ImageBackground source={HorseVideoIcon} style={{ width: "100%", height: "100%" }}>
                    <View style={{ flex: 2.5, backgroundColor: '#2080BD20', paddingVertical: normalizeWidth(5), paddingHorizontal: normalizeWidth(5) }}>
                        <BackHeader heading={heading} useWhiteTheme={useWhiteTheme} />
                    </View>
                </ImageBackground>
            </View>
            <View style={{ flex: 2.5 }}>
                {children}
            </View>
        </View >
    )
}

const styles = StyleSheet.create({
    background: {
        // width: '100%',
        height: "30%",
        borderBottomLeftRadius: 200,
        borderBottomRightRadius: 200,
        // flexDirection: 'row',
        flex: 1
    }
})

export default BackgroundImageHeader;