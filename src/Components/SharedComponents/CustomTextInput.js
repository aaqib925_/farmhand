import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import React, { useRef, useState } from "react"
import { TextInput, View, StyleSheet, Text, Platform, Image } from "react-native"
import { TouchableOpacity } from "react-native-gesture-handler"
import { containerShadows, errorColor, fontFamily, fontH4, TextInputBackgroundColor, TextInputFontColor } from "../../Constants/styles"
import { normalizeFont, normalizeHeight, normalizeWidth, normalizeWithScale } from "../../Utils/fontUtil"

const CustomTextInput = ({ attrName, title = "", leftIcon, value, readOnly, error, updateMasterState, customIcon, showEye, keyboardType, autoCapitalize, onFocus, onBlur, rightIcon = null, borderRadius = 20, backgroundColor = TextInputBackgroundColor }) => {
    const [isSecure, setIsSecure] = useState(true);
    const [isFocused, setIsFocused] = useState(false)
    const handleOnTextChange = (text) => {
        updateMasterState(text, attrName)
    }
    const handleFocus = () => {
        setIsFocused(true)
        if (onFocus) {
            onFocus(attrName);
        }
    }
    const handleBlur = () => {
        setIsFocused(false)
        if (onBlur) {
            onBlur(attrName);
        }
    }
    return (
        <View>
            <View style={{ ...styles.container, borderColor: error ? errorColor : isFocused ? "#2080BD" : "#42526E80", borderBottomWidth: StyleSheet.hairlineWidth }}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    {
                        leftIcon &&
                        <View>
                            <Image source={leftIcon} style={{ width: normalizeWidth(6), height: normalizeWidth(6) }} resizeMode={"contain"} />
                        </View>
                    }
                </View >
                <View style={{ flex: 7, justifyContent: 'center' }}>
                    <TextInput
                        name={attrName}
                        placeholder={title}
                        fontSize={normalizeFont(14)}
                        placeholderTextColor={TextInputFontColor}
                        value={value}
                        style={styles.textStyle}
                        underlineColorAndroid="transparent"
                        editable={readOnly ? false : true}
                        onChangeText={handleOnTextChange}
                        secureTextEntry={showEye && isSecure ? true : false}
                        keyboardType={keyboardType}
                        autoCapitalize={autoCapitalize}
                        onFocus={handleFocus}
                        onBlur={handleBlur}
                        fontFamily={fontFamily.Primary.Regular}
                    />
                </View>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', }}>
                    {
                        showEye && isSecure &&

                        <TouchableOpacity onPress={() => setIsSecure(!isSecure)}>
                            <FontAwesomeIcon size={normalizeWithScale(15)} color={'#2080BD'} icon={faEyeSlash} />
                            {/* <Image source={eye} /> */}
                        </TouchableOpacity>

                    }
                    {
                        showEye && !isSecure &&

                        <TouchableOpacity onPress={() => setIsSecure(!isSecure)}>
                            <FontAwesomeIcon size={normalizeWithScale(15)} color={'#2080BD'} icon={faEye} />
                            {/* <Image source={eye} /> */}
                        </TouchableOpacity>

                    }
                    {
                        customIcon &&
                        <View >
                            <FontAwesomeIcon size={normalizeWithScale(15)} color={'#2080BD'} icon={customIcon} />
                        </View>
                    }
                    {
                        rightIcon &&
                        <View>
                            <Image source={rightIcon} style={{ width: normalizeWidth(15), height: normalizeHeight(15) }} resizeMode={"contain"} />
                        </View>
                    }

                </View>

            </View>
            {
                error &&
                <View style={{ alignItems: 'center' }}>
                    <Text style={{ color: errorColor, fontSize: fontH4, fontFamily: fontFamily.Primary.Regular, }}>{error}</Text>
                </View>

            }
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: TextInputBackgroundColor,
        height: normalizeHeight(50),
        marginVertical: normalizeHeight(10),
        flexDirection: 'row',
    },
    textStyle: {
        color: TextInputFontColor,
        width: '85%',
        borderWidth: 0,
        borderColor: 'white'
    },
    iconStyle: {
        marginRight: normalizeWidth(10)
    }
})
export default CustomTextInput;