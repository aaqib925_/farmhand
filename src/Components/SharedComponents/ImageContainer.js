import React, { useState } from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { primaryColor } from '../../Constants/styles';
import { normalizeWidth } from '../../Utils/fontUtil';
import Loading from './Loading';
const ImageContainer = ({ image, circleWidth = 100, backgroundColor = 'white', loaderColor = primaryColor, loaderSize = "large", borderRadius = null, borderColor = "white", borderWidth = 0, styles = {} }) => {
    const [imageLoading, setImageLoading] = useState(false)
    return (
        <View style={{ alignItems: 'center', position: 'relative' }}>
            <View style={{
                width: normalizeWidth(circleWidth),
                backgroundColor: backgroundColor,
                height: normalizeWidth(circleWidth),
                borderRadius: borderRadius ? normalizeWidth(borderRadius) : normalizeWidth(circleWidth / 2),
                borderColor: borderColor,
                borderWidth: borderWidth,
                overflow: "hidden",
                ...styles
            }}>
                {
                    imageLoading ?
                        <View style={{ ...StyleSheet.absoluteFill, justifyContent: 'center' }}>
                            <Loading loading={true} size={loaderSize} color={loaderColor} />
                        </View> : null

                }
                <Image
                    source={image}
                    style={{ width: "100%", height: "100%" }}
                    resizeMode={'cover'}
                    onLoadStart={e => setImageLoading(true)}
                    onLoad={e => setImageLoading(false)}
                    onLoadEnd={e => setImageLoading(false)}
                />
            </View>
        </View>
    )
}

export default ImageContainer