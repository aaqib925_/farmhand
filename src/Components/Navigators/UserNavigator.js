import React, { useState, useEffect } from "react";
import { FlatList, Image, Pressable, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { RouteNames } from "../../Constants/routeNames"
import { createStackNavigator } from "@react-navigation/stack";
import { normalizeHeight, normalizeWidth } from "../../Utils/fontUtil";
import { fontFamily, fontH1, fontH2, fontH2V3, fontH3, fontH3V3, primaryColor, secondaryColor, textPrimayColor, textSecondaryColor, textYellowColor } from "../../Constants/styles";
import Dashboard from "../Views/UserViews/Dashboard";
import AnimalTypeScreen from "../Views/UserViews/AnimalTypeScreen";
import AnimalDashboardScreen from "../Views/UserViews/AnimalDashboardScreen";
import AnimalInventoryScreen from "../Views/UserViews/AnimalInventoryScreen";
import CertificateScreen from "../Views/UserViews/CertificateScreen";
import AddCertificateScreen from "../Views/UserViews/AddCertificateScreen";
import VideoArchieveScreen from "../Views/UserViews/VideoArchieveScreen";
import SubscriptionScreen from "../Views/UserViews/SubscriptionScreen";
import SettingsScreen from "../Views/UserViews/SettingsScreen";
import MenuDrawer from 'react-native-side-drawer'
import { cross, HorseImage } from "../../assets/images";
import ImageContainer from "../SharedComponents/ImageContainer";
import { dummyNotification, menuProfile, NotificationType } from "../../Constants/enum";
import { useNavigation } from "@react-navigation/core";
import AddVideoScreen from "../Views/UserViews/AddVideoScreen";
const Stack = createStackNavigator();
const UserNavigator = (props) => {
    const navigation = useNavigation()
    useEffect(() => {
        navigation.setParams({ toggleMenuDrawer: toggleMenuDrawer, toggleNotificationDrawer: toggleNotificationDrawer })
    }, [])
    const [isMenuDrawerOpen, setIsMenuDrawerOpen] = useState(false);
    const [isNotificationDrawerOpen, setIsNotificationDrawerOpen] = useState(false)
    const toggleMenuDrawer = () => {
        setIsMenuDrawerOpen(!isMenuDrawerOpen)
    }
    const toggleNotificationDrawer = () => {
        console.log("REACHED****")
        setIsNotificationDrawerOpen(!isNotificationDrawerOpen)
    }
    const handleRedirection = (route, params) => {
        navigation.navigate(route, { ...params })
        toggleMenuDrawer()
    }
    const renderItem = ({ item }) => {
        return (
            <Pressable onPress={() => handleRedirection(item.navigationScreen)} style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginBottom: normalizeHeight(35) }}>
                <Image source={item.icon} style={{ width: normalizeWidth(8), height: normalizeWidth(8), resizeMode: 'contain', marginRight: normalizeWidth(7) }} />
                <Text style={{ fontFamily: fontFamily.Primary.Medium, color: "#42526E", fontSize: fontH2V3 }}>{item.title}</Text>
            </Pressable>
        )
    }

    const drawerContent = () => {
        return (
            <View style={styles.animatedBox}>
                <View >
                    <TouchableOpacity onPress={toggleMenuDrawer}>
                        <Image source={cross} style={{ width: normalizeWidth(4), height: normalizeWidth(4), resizeMode: 'contain', padding: normalizeWidth(3) }} />
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1, }}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <ImageContainer image={HorseImage} circleWidth={50} />
                        <Text style={{ color: textPrimayColor, fontFamily: fontFamily.Primary.Bold, fontSize: fontH2 }}>Dave Parker</Text>
                        <Text style={{ color: textSecondaryColor, fontFamily: fontFamily.Primary.Regular, fontSize: fontH3 }}>Info@acsquare.com</Text>
                    </View>
                    <View style={{ flex: 1.5, marginTop: normalizeHeight(50), justifyContent: 'center', alignItems: 'center' }}>
                        <FlatList
                            data={menuProfile}
                            keyExtractor={(item, index) => item.id}
                            renderItem={renderItem}
                            scrollEnabled={false}
                        />
                    </View>
                </View>
            </View>
        )
    }
    const renderNotificationItem = ({ item, index }) => {
        return (
            <View style={{ paddingVertical: normalizeHeight(25), borderBottomWidth: index === dummyNotification.length - 1 ? null : StyleSheet.hairlineWidth, borderBottomColor: "#42526E3B" }}>
                <View style={{ paddingHorizontal: normalizeWidth(3) }}>
                    <Text style={{ marginBottom: normalizeHeight(5), color: item.type === NotificationType.Vaccination ? secondaryColor : textYellowColor, fontFamily: fontFamily.Primary.Heavy, fontSize: fontH3 }}>{item.type}</Text>
                    <Text style={{ marginBottom: normalizeHeight(5), color: textPrimayColor, fontSize: fontH2, fontFamily: fontFamily.Primary.Heavy }}>{item.name}</Text>
                    <Text style={{ marginBottom: normalizeHeight(5), color: textSecondaryColor, fontSize: fontH2V3, fontFamily: fontFamily.Primary.Regular }}>{item.date}</Text>
                </View>
            </View>
        )
    }
    const NotifcationDrawerContent = () => {
        return (
            <View style={styles.animatedBox}>
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={toggleNotificationDrawer} style={{ position: 'absolute', left: 0, top: normalizeWidth(2) }}>
                        <Image source={cross} style={{ width: normalizeWidth(7), height: normalizeWidth(7), resizeMode: 'contain', padding: normalizeWidth(4) }} />
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ justifyContent: 'center', fontSize: fontH2, fontFamily: fontFamily.Primary.Bold, color: textPrimayColor }}>Notification</Text>
                        <View style={{ marginLeft: normalizeWidth(2), width: normalizeWidth(10), height: normalizeWidth(10), backgroundColor: "#F94F4F", borderRadius: normalizeWidth(5), justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: 'white', fontSize: fontH3, fontFamily: fontFamily.Primary.Medium }}>2</Text>
                        </View>
                    </View>
                </View>
                <View style={{ flex: 2, marginTop: normalizeWidth(25) }}>
                    <View>
                        <FlatList
                            data={dummyNotification}
                            keyExtractor={(item, index) => item.id}
                            renderItem={renderNotificationItem}
                        />
                    </View>
                </View>
            </View>
        )
    }
    return (
        <MenuDrawer
            open={isMenuDrawerOpen}
            drawerContent={drawerContent()}
            drawerPercentage={45}
            animationTime={250}
            position={'left'}
            overlay={true}
            opacity={0.4}>
            <MenuDrawer
                open={isNotificationDrawerOpen}
                drawerContent={NotifcationDrawerContent()}
                drawerPercentage={45}
                animationTime={250}
                position={'right'}
                overlay={true}
                opacity={0.4}>
                <Stack.Navigator
                    screenOptions={{
                        presentation: 'modal',
                        // animationEnabled: false,
                        cardStyleInterpolator: ({ current, layouts }) => {
                            return {
                                cardStyle: {
                                    transform: [
                                        {
                                            translateX: current.progress.interpolate({
                                                inputRange: [0, 1],
                                                outputRange: [layouts.screen.width, 0],
                                            }),
                                        },
                                    ],
                                },
                            };
                        },
                        cardStyle: { backgroundColor: "white" },
                        headerStyle: {
                            backgroundColor: secondaryColor,
                        },
                        cardOverlay: () => (
                            <View
                                style={{
                                    flex: 1,
                                    backgroundColor: "white",
                                }}
                            />),
                        header: (props) => {
                            let state = props.navigation.dangerouslyGetState();
                            return (
                                null
                            )
                        },

                    }}
                >

                    <Stack.Screen
                        name={RouteNames.User.Dashboard}
                        component={Dashboard}
                        options={{ headerShown: false }}
                    />
                    <Stack.Screen
                        name={RouteNames.User.AddVideo}
                        component={AddVideoScreen}
                        options={{ headerShown: false }}
                    />
                    <Stack.Screen
                        name={RouteNames.User.AddAnimalCertificate}
                        component={AddCertificateScreen}
                        options={{ headerShown: false }}
                    />
                    <Stack.Screen
                        name={RouteNames.User.Settings}
                        component={SettingsScreen}
                        options={{ headerShown: false }}
                    />
                    <Stack.Screen
                        name={RouteNames.User.SubscriptionScreen}
                        component={SubscriptionScreen}
                        options={{ headerShown: false }}
                    />
                    <Stack.Screen
                        name={RouteNames.User.AnimalInventory}
                        component={AnimalInventoryScreen}
                        options={{ headerShown: false }}
                    />

                    <Stack.Screen
                        name={RouteNames.User.VideoArchieve}
                        component={VideoArchieveScreen}
                        options={{ headerShown: false }}
                    />

                    <Stack.Screen
                        name={RouteNames.User.AnimalCertificate}
                        component={CertificateScreen}
                        options={{ headerShown: false }}
                    />


                    <Stack.Screen
                        name={RouteNames.User.AnimalDashboard}
                        component={AnimalDashboardScreen}
                        options={{ headerShown: false }}
                    />
                    <Stack.Screen
                        name={RouteNames.User.AnimalType}
                        component={AnimalTypeScreen}
                        options={{ headerShown: false }}
                    />


                </Stack.Navigator >
            </MenuDrawer>
        </MenuDrawer>

    )
}
const styles = StyleSheet.create({
    stack: {
        flex: 2,
        backgroundColor: 'white',
        shadowColor: 'white',
        shadowOffset: {
            width: normalizeWidth(0),
            height: normalizeHeight(8),
        },
        shadowOpacity: 0.44,
        shadowRadius: 10.32,
        elevation: 50,
        color: "white",
    },
    animatedBox: {
        flex: 1,
        backgroundColor: "white",
        margin: normalizeWidth(7),
        marginTop: normalizeWidth(12),
        borderRadius: normalizeWidth(5),
        overflow: 'hidden',
        padding: normalizeWidth(8)
    },

});

export default UserNavigator