import React from 'react';
import { View } from 'react-native'
import { createStackNavigator } from "@react-navigation/stack"
import { fontFamily, fontH2V2 } from '../../Constants/styles'
import { primaryColor } from '../../Constants/styles'
import { RouteNames } from "../../Constants/routeNames"
import Login from '../Views/AuthViews/Login';
const Stack = createStackNavigator();
const AuthNavigator = (props) => {
    return (
        <Stack.Navigator
            screenOptions={{
                presentation: 'modal',
                cardStyleInterpolator: ({ current, layouts }) => {
                    return {
                        cardStyle: {
                            transform: [
                                {
                                    translateX: current.progress.interpolate({
                                        inputRange: [0, 1],
                                        outputRange: [layouts.screen.width, 0],
                                    }),
                                },
                            ],
                        },
                    };
                },
                cardOverlay: () => (
                    <View
                        style={{
                            flex: 1,
                            backgroundColor: "white",
                        }}
                    />),
                cardStyle: {
                    backgroundColor: "white"
                },
                headerStyle: {
                    backgroundColor: '#fff',

                },
                headerTitleStyle: { fontFamily: fontFamily.Primary.SemiBold, fontSize: fontH2V2 },
            }}>

            <Stack.Screen
                name={RouteNames.AuthRoutes.Login}
                component={Login}
                options={{ headerShown: false }}
            />


        </Stack.Navigator>

    )
}

export default AuthNavigator