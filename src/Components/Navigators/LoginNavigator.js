import React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { RouteNames } from "../../Constants/routeNames"
import LoginTab from '../Views/AuthViews/Tabs/loginTab';
import SignupTab from '../Views/AuthViews/Tabs/signup';
const Tab = createMaterialTopTabNavigator();
const LoginTabs = () => {
    return (
        <Tab.Navigator>
            <Tab.Screen name={RouteNames.AuthRoutes.SignIn} component={LoginTab} />
            <Tab.Screen name={RouteNames.AuthRoutes.SignUp} component={SignupTab} />
        </Tab.Navigator>
    );
}

export default LoginTabs