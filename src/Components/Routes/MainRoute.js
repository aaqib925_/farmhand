import { NavigationContainer } from '@react-navigation/native';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import AuthNavigator from '../Navigators/AuthNavigator';
import UserNavigator from '../Navigators/UserNavigator';
const MainRoute = ({ authorize, firstLogin }) => {

    return (

        <NavigationContainer>
            {
                authorize ? <UserNavigator /> : <AuthNavigator />
            }
        </NavigationContainer>
    )
}
const mapDispatchToProps = {

};
const mapStateToProps = state => {
    return {
        authorize: state.auth.authorize,
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(MainRoute)

