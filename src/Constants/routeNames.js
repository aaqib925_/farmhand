export const RouteNames = {
    "AuthRoutes": {
        "Login": "Login",
        "SignIn": "SignIn",
        "SignUp": "SignUp",
    },
    "User": {
        "Dashboard": "Dashboard",
        "AnimalType": "AnimalType",
        "AnimalDashboard": "AnimalDashboard",
        "AnimalInventory": "AnimalInventory",
        "AnimalCertificate": "AnimalCertificate",
        "AddAnimalCertificate": "AddAnimalCertificate",
        "VideoArchieve": "VideoArchieve",
        "SubscriptionScreen": "SubscriptionScreen",
        "Settings": "Settings",
        "AddVideo": "AddVideo",
    },
}