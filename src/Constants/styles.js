import { normalizeFont, normalizeHeight, normalizeWidth } from "../Utils/fontUtil";
import { Platform } from "react-native"
// FontFamily throughout the applications
export const fontFamily = {
    "Primary": {
        "Medium": Platform.OS === 'ios' ? "FuturaPT-Medium" : "FuturaPTMedium",
        "Regular": Platform.OS === 'ios' ? "FuturaPT-Book" : "FuturaPTBook",
        "Light": Platform.OS === 'ios' ? "FuturaPT-Light" : "FuturaPTLight",
        "Bold": Platform.OS === 'ios' ? "FuturaPT-Bold" : "FuturaPTBold",
        "Heavy": Platform.OS === 'ios' ? "FuturaPT-Heavy" : "FuturaPTHeavy"
    }
}
// Standard Colors throughout the application must be used from here.

export const primaryColor = "#15689C";
export const secondaryColor = "#2080BD"
export const secondaryColorWithOptacity = '#EEF4F7'
export const inActiveColor = "#707070";
export const greyedSchemeColor = "#00000050";
export const errorColor = "red";
export const buttonPrimary = "#2080BD"
export const textPrimayColor = "#0E134F"
export const textSecondaryColor = "#42526EB2"
export const textYellowColor = "#F7941D"
// Standard Font Sizes throughout the application must be used from here

export const fontH1 = normalizeFont(24);
export const fontH2 = normalizeFont(18);
export const fontH2V2 = normalizeFont(15);
export const fontH2V3 = normalizeFont(13)
export const fontH3 = normalizeFont(12);
export const fontH3V3 = normalizeFont(10);
export const fontH4 = normalizeFont(8);
export const fontSmallSize = normalizeFont(6);


// Generalized styles shall be listed here.
export const mainContainer = {
    flex: 1,
    padding: normalizeWidth(10),
    justifyContent: 'center',
    alignItems: 'center'
}

export const quoteStyles = {
    fontFamily: fontFamily.Primary.Regular,
    fontSize: fontH2
}

export const containerShadow = {
    shadowColor: "gray",
    shadowOffset: {
        width: 0,
        height: 6,
    },
    shadowOpacity: 0.39,
    shadowRadius: 8.30,

    elevation: 13,
}

export const SettingsContainerStyles = {
    backgroundColor: 'white',
    ...containerShadow,
    borderRadius: normalizeWidth(3),
    paddingVertical: normalizeHeight(20),
    paddingHorizontal: normalizeWidth(7)
}

export const SettingsContainerHeadingStyle = {
    fontFamily: fontFamily.Primary.Heavy,
    fontSize: fontH3,
    color: textPrimayColor
}
