// All enums of the applications shall be listed here *********

import { CertificateExample1, CertificateExample2, CertificateExample3, dashboardBlueIcon, DashboardIconImage, HorseHeaderBackgroundImage, logoutIcon, settings, settingsIcon, SubscriptionPlanIcon } from "../assets/images"
import EmailSettings from "../Components/Views/UserViews/Settings/EmailSettings"
import LanguageSettings from "../Components/Views/UserViews/Settings/LanguageSettings"
import PhoneSettings from "../Components/Views/UserViews/Settings/PhoneSettings"
import { RouteNames } from "./routeNames"

export const languageLocale = {
    zh: 'zh',
    en: 'en'
}

export const AnimalInventoryData = [
    {
        id: 1,
        title: "Name",
        value: "Alex",
    },
    {
        id: 2,
        title: "Registry",
        value: "AQHA",
    },
    {
        id: 3,
        title: "Birthday",
        value: "March 2, 1999",
    },
    {
        id: 4,
        title: "Birth Place",
        value: "King Ranch",
    },
]

export const inventoryVideoArchives = [
    {
        id: 0,
        showAdd: true
    },
    {
        id: 1,
        title: "Shiw Visit",
        date: "5/18/2019"
    },
    {
        id: 1,
        title: "Vet Visit",
        date: "5/18/2019"
    },
    {
        id: 2,
        title: "Spring Review",
        date: "5/18/2019"
    },
    {
        id: 3,
        title: "Autumn Review",
        date: "5/18/2019"
    },
    {
        id: 4,
        title: "Autumn Review",
        date: "5/18/2019"
    },
    {
        id: 5,
        title: "Autumn Review",
        date: "5/18/2019"
    },
    {
        id: 6,
        title: "Autumn Review",
        date: "5/18/2019"
    },
    {
        id: 7,
        title: "Autumn Review",
        date: "5/18/2019"
    }
]
export const dummyVideoArchives = [
    {
        id: 1,
        title: "Shiw Visit",
        date: "5/18/2019"
    },
    {
        id: 1,
        title: "Vet Visit",
        date: "5/18/2019"
    },
    {
        id: 2,
        title: "Spring Review",
        date: "5/18/2019"
    },
    {
        id: 3,
        title: "Autumn Review",
        date: "5/18/2019"
    },
    {
        id: 4,
        title: "Autumn Review",
        date: "5/18/2019"
    },
    {
        id: 5,
        title: "Autumn Review",
        date: "5/18/2019"
    },
    {
        id: 6,
        title: "Autumn Review",
        date: "5/18/2019"
    },
    {
        id: 7,
        title: "Autumn Review",
        date: "5/18/2019"
    }
]

export const InventoryReminderData = [
    {
        id: 1,
        name: "5 ml. Adequan",
        time: "Once Daily(5 Days)"
    },
    {
        id: 1,
        name: "Amoxicillin 500 mg",
        time: "Once Daily(5 Days)"
    },
    {
        id: 1,
        name: "Tetanus",
        time: "Once Daily(5 Days)"
    },
]

export const inventoryHealthData = [
    {
        id: 1,
        name: "Tetanus",
        time: "Due-05/18/2019",
        type: "Vaccination",
        color: '#F94F4F'
    },
    {
        id: 2,
        name: "Farrier",
        time: "Due-05/18/2019",
        type: "Vaccination",
        color: '#65CE63'
    },
    {
        id: 3,
        name: "Strangles (Becky)",
        time: "Due-05/18/2019",
        type: "Vaccination",
        color: '#F7941D'
    },
]

export const DummyCertificates = [
    {
        id: 1,
        image: CertificateExample1
    },
    {
        id: 2,
        image: CertificateExample2
    },
    {
        id: 3,
        image: CertificateExample3
    },
    {
        id: 4,
        image: CertificateExample3
    },
]

export const menuProfile = [
    {
        id: 1,
        title: "Dashboard",
        icon: dashboardBlueIcon,
        navigationScreen: RouteNames.User.Dashboard
    },
    {
        id: 2,
        title: "Subscription Plan",
        icon: SubscriptionPlanIcon,
        navigationScreen: RouteNames.User.SubscriptionScreen
    },
    {
        id: 3,
        title: "Settings",
        icon: settingsIcon,
        navigationScreen: RouteNames.User.Settings
    },
    {
        id: 4,
        title: "Logout",
        icon: logoutIcon,
        navigationScreen: RouteNames.User.Dashboard
    },
]

export const SettingsMenu = [
    {
        id: 1,
        title: "Email",
        isLast: false,
        component: EmailSettings
    },
    {
        id: 2,
        title: "Phone",
        isLast: false,
        component: PhoneSettings
    },
    {
        id: 3,
        title: "Language",
        isLast: true,
        component: LanguageSettings
    },
]

export const languageData = [
    {
        id: 1,
        name: "English",
        selected: true,
    },
    {
        id: 2,
        name: "Spanish",
        selected: true,
    },
    {
        id: 3,
        name: "French",
        selected: true,
    },
    {
        id: 4,
        name: "English UK",
        selected: true,
    },
]

export const NotificationType = {
    "Vaccination": "Vaccination",
    "Appointment": "Appointment"
}

export const dummyNotification = [
    {
        id: 1,
        type: NotificationType.Appointment,
        name: "Tetanus (Jackson)",
        date: "Due: 05/18/2019"
    },
    {
        id: 2,
        type: NotificationType.Vaccination,
        name: "Tetanus (Jackson)",
        date: "Due: 05/18/2019"
    },
    {
        id: 3,
        type: NotificationType.Vaccination,
        name: "Tetanus (Jackson)",
        date: "Due: 05/18/2019"
    },
    {
        id: 4,
        type: NotificationType.Vaccination,
        name: "Tetanus (Jackson)",
        date: "Due: 05/18/2019"
    },
    {
        id: 5,
        type: NotificationType.Vaccination,
        name: "Tetanus (Jackson)",
        date: "Due: 05/18/2019"
    },
]