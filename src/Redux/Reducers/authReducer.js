import { LOGIN_SUCCESSFUL } from "../Actions/authAction"

const initialState = {
    authorize: true
}

export default (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_SUCCESSFUL: {
            console.log({
                ...state,
            })
            return {
                ...state,
                authorize : true
            }
        }
        default:
            return {
                ...state
            }
    }
}