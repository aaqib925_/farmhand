import { combineReducers } from "redux";
import AuthReducer from "./authReducer";
import TabBarReducer from './TabBarReducer'
const appReducer = combineReducers({
    TabBar: TabBarReducer,
    auth: AuthReducer
})

export default appReducer;