import BackgroundImage from "./bg-img.png";
import LogoWithTextImage from "./logo1.png";
import DashboardIconImage from "./dashboard.png";
import AwayCareImage from "./away-care.png";
import VetChatImage from "./vet-chat.png";
import HealthRemainderImage from "./health-reminders.png";
import BreedingRecordsImage from "./breeding-records.png";
import MedicalRecordsImage from "./medical-records.png";
import RacingImage from "./racing.png";
import MenuIcon from "./hamburger-menu.png";
import NotificationWhiteIconImage from "./notification-white.png";
import HeaderPlusIconImage from "./header-plus.png";
import BackColoredIcon from "./angle-back.png"
import NextColoredIcon from "./angle-next.png"
import NotificationColoredIcon from "./notification.png"
import HeaderPlusColoredIcon from "./vet-doctor.png"
import CowImage from "./cow-image.png"
import HourseImage from "./horse-image.png"
import SwineImage from "./swine-image.png"
import EmailIcon from "./email.png"
import ButtonNextIcon from "./btn-next.png"
import LockIcon from "./password.png"
import user from "./user.png"
import annoucementImage from "./announcements.png"
import addPlusPlaceHolder from "./add.png"
import HorseImage from "./horseImage.png"
import PlayIcon from "./playButton.png"
import HorseVideoIcon from "./horseVideo.png"
import bluePlusIcon from "./bluePlusIcon.png"
import TickImage from "./tick.png"
import HorseHeaderBackgroundImage from "./horse-header-background.png"
import backButtonWhite from "./backButtonWhite.png";
import bellWhite from "./bellWhite.png";
import HeaderPlusColoredIconWhite from "./HeaderPlusColoredIconWhite.png"
import OrangePlusIcon from "./brn-add.png"
import CertificateExample2 from "./CertificateExample2.png";
import CertificateExample3 from "./CertificateExample3.png"
import CertificateExample1 from "./CertificateExample1.png"
import cross from "./cross.png"
import settingsIcon from "./settings.png"
import SubscriptionPlanIcon from "./subscription-plan.png"
import logoutIcon from "./logout.png"
import dashboardBlueIcon from "./dashboardBlueIcon.png"
import CheckBoxInActive from "./checkbox-inactive.png";
import CheckBoxActive from "./checkbox.png"
import OrangeCheckIcon from "./btn-check.png"
export {
    addPlusPlaceHolder,
    annoucementImage,
    SwineImage,
    HourseImage,
    CowImage,
    BackColoredIcon,
    NotificationColoredIcon,
    HeaderPlusColoredIcon,
    HeaderPlusIconImage,
    NotificationWhiteIconImage,
    MenuIcon,
    BackgroundImage,
    LogoWithTextImage,
    DashboardIconImage,
    AwayCareImage,
    VetChatImage,
    HealthRemainderImage,
    BreedingRecordsImage,
    MedicalRecordsImage,
    RacingImage,
    EmailIcon,
    ButtonNextIcon,
    LockIcon,
    user,
    NextColoredIcon,
    HorseImage,
    PlayIcon,
    HorseVideoIcon,
    bluePlusIcon,
    TickImage,
    HorseHeaderBackgroundImage,
    HeaderPlusColoredIconWhite,
    backButtonWhite,
    bellWhite,
    OrangePlusIcon,
    CertificateExample1,
    CertificateExample2,
    CertificateExample3,
    cross,
    settingsIcon,
    SubscriptionPlanIcon,
    logoutIcon,
    dashboardBlueIcon,
    CheckBoxInActive,
    CheckBoxActive,
    OrangeCheckIcon
}